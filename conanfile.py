from conans import ConanFile, CMake, tools
import shutil

class AkumuliConan(ConanFile):
    name            = "akumuli"
    version         = "0.8.79"
    license         = "Apache-2.0"
    author          = "toge.mail@gmail.com"
    url             = "https://bitbucket.org/toge/conan-akumuli/"
    homepage        = "https://github.com/akumuli/Akumuli"
    description     = "Akumuli is a time-series database for modern hardware."
    topics          = ("time-series", "database", "column-oriented")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = {"shared": False}
    generators      = "cmake"
    requires        = [
                        "boost/[>= 1.5.0]",
                        "log4cxx/0.10.0_20200504@toge/stable",
                        "sqlite3/3.31.0",
                        "apr/1.7.0",
                        "apr-util/1.6.1",
                        "libmicrohttpd/0.9.59@bincrafters/stable",
                        "muparser/2.2.6@conan/stable"
                    ]

    def source(self):
        tools.get("https://github.com/akumuli/Akumuli/archive/v{}.zip".format(self.version))
        shutil.move("Akumuli-{}".format(self.version), "Akumuli")

        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file("Akumuli/CMakeLists.txt", "project(Akumuli)",
                              '''project(Akumuli)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

        tools.replace_in_file("Akumuli/CMakeLists.txt", "-Werror", "")
        tools.replace_in_file("Akumuli/libakumuli/storage2.h", "#include <vector>", '''#include <vector>
#include <stack>''')

        if not self.options.shared:
            tools.replace_in_file("Akumuli/libakumuli/CMakeLists.txt", "add_library(akumuli SHARED", "add_library(akumuli STATIC")

    def configure(self):
        self.options["apr-util"].with_sqlite3 = True

    def build(self):
        cmake = CMake(self)
        cmake.verbose = True
        cmake.configure(source_folder="Akumuli")
        cmake.build(target="akumuli")

    def package(self):
        self.copy("*.h", dst="include", src="Akumuli/include")
        if self.settings.os == "Windows":
            self.copy("*.lib",   dst="lib",     keep_path=False)
            self.copy("*.dll",   dst="bin",     keep_path=False)
        if self.settings.os == "Linux":
            self.copy("*.so",    dst="lib",     keep_path=False)
            self.copy("*.a",     dst="lib",     keep_path=False)
        if self.settings.os == "Macos":
            self.copy("*.so",    dst="lib",     keep_path=False)
            self.copy("*.a",     dst="lib",     keep_path=False)
            self.copy("*.dylib", dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["akumuli", "roaring", "lz4"]
